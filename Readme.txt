Please perform the following before executing Automated Unit tests

1) Make sure that you have a valid account on https://cloud.aspose.com/

2) Copy and paste your AppSid and AppKey in "Aspose.CloudTests/Utils.cs". You can get AppSid and AppKey from https://cloud.aspose.com/

3) Create the following two folders at the root on https://cloud.aspose.com/#/files
	1) SDKTest
	2) SDKTestOutput

4) Upload all the files found in "Aspose.CloudTests\TestData" to Aspose Cloud 'SDKTest' folder created in step 2.


Note: Please do not run all unit tests at once as it can largely consume your Aspose Cloud Plan space and usage limits. In case you get time-out exception on any method; it does not necessarily mean there is a problem with the method, executing that test again will most likely fix the issue.

Please feel free to contact us at marketplace@aspose.com for any issues or suggestions.